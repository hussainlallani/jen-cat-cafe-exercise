import "./App.css";
import { Route } from "react-router-dom";
import CatProfile from "./CatProfile/CatProfile";
import OneCat from "./CatProfile/OneCat";
import AddCatForm from './AddCatForm/AddCatForm'


function App() {
  return (
    <div className="App">
      <Route exact path="/cats" component={CatProfile} />
      <Route exact path="/cats/:id" component={OneCat} />
      <Route exact path="/add-cat" component={AddCatForm} />
    </div>
  );
}

export default App;
